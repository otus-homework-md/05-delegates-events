﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dellegates05.Infrastructure.Interfaces;

internal interface IController
{
    void Start();
}
