﻿
using dellegates05.BL.implementeion;
using dellegates05.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dellegates05.Infrastructure.Implementation;

internal class Controller : IController
{
    public void Start()
    {
        var proc=new processor(Environment.CurrentDirectory, 15000);

        proc.FileFound += Proc_FileFound;

        proc.SearchFileInDir();

        Console.WriteLine($"--------\nСамый длинный файл(по названию):\n{proc.ProcessFiles()}");

        do { } while (!proc.isStopProcess);
    }

    private void Proc_FileFound(object? sender, FileArgs e)
    {
        var mes = e.WhatHappenWithFile == FileOperation.none ? "Найден файл:" :
                  e.WhatHappenWithFile == FileOperation.create ? "Создан файл:":
                  e.WhatHappenWithFile == FileOperation.update ? "Обновлен файл:" :
                  e.WhatHappenWithFile == FileOperation.renamed ? "Переименован файл:" :
                  e.WhatHappenWithFile == FileOperation.delete ? "Удален файл:" :"неизвестная операция с файлом";
        Console.WriteLine($"-------------\n{mes}\n{e.FileName}");
    }
}
