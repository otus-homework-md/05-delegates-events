﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dellegates05.BL.extension;

public static class Extension
{

    public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter)  where T : class
    {
        T? result=null;

        var enumerator=e.GetEnumerator();
        enumerator.Reset();

        float? maxVal=null;

        //Console.WriteLine($"__________________________________");

        while (enumerator.MoveNext())
        {
            if (maxVal.HasValue)
            {
                if(getParameter((T)enumerator.Current) > maxVal)
                {
                    maxVal = getParameter((T)enumerator.Current);
                    result = (T)enumerator.Current;
                }
                

                //Console.WriteLine($"{result} - {maxVal}");
            }
            else
            {
                maxVal=getParameter((T)enumerator.Current);
                result=(T)enumerator.Current;
                //Console.WriteLine($"{result} - {maxVal}");
            }
        }

        return result;

    }



    public static float ObjectToFloat<T>(T obj) where T : class
    {
        return obj?.ToString().Length ?? 0;
    }

}
