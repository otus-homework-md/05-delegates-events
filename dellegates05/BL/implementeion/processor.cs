﻿using System.Timers;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dellegates05.BL.extension;

namespace dellegates05.BL.implementeion;


internal class processor
{
    System.Timers.Timer timer = new System.Timers.Timer();
    FileSystemWatcher fileSystemWatcher = new System.IO.FileSystemWatcher();
    List<String> files = new List<String>();
    String? workingDir = null;
    Double? maxTime = null;
    public volatile bool isStopProcess = false;

    public delegate void EventHandler(object? sender, FileArgs e);
    public event EventHandler? FileFound;


    public processor(String _dir, Double _maxTime)
    {
        workingDir = _dir;
        maxTime = _maxTime;
        var currentDirectory = Environment.CurrentDirectory;        

        files = Directory.EnumerateFiles(workingDir).ToList();

        timer.Interval = maxTime.Value;

        timer.Elapsed += Timer_Elapsed;       

        timer.Start();

        fileSystemWatcher.Path = workingDir;
        fileSystemWatcher.EnableRaisingEvents = true;
        //fileSystemWatcher.Changed += FileSystemWatcher_Changed;
        fileSystemWatcher.Created += FileSystemWatcher_Created;
        fileSystemWatcher.Deleted += FileSystemWatcher_Deleted;
        fileSystemWatcher.Renamed += FileSystemWatcher_Renamed;

    }

    private void FileSystemWatcher_Renamed(object sender, RenamedEventArgs e)
    {
        FileFound?.Invoke(this, new FileArgs(e.FullPath, FileOperation.renamed));

        files = Directory.EnumerateFiles(workingDir).ToList();

        Console.WriteLine($"------------\nПроизошло событие FileSystemWatcher_Renamed\nВ данный момент самый длинный файл в директории : \n{ProcessFiles()}");
    }

    private void FileSystemWatcher_Deleted(object sender, FileSystemEventArgs e)
    {
        FileFound?.Invoke(this, new FileArgs(e.FullPath, FileOperation.delete));

        files = Directory.EnumerateFiles(workingDir).ToList();

        Console.WriteLine($"------------\nПроизошло событие FileSystemWatcher_Deleted\nВ данный момент самый длинный файл в директории : \n{ProcessFiles()}");
    }

    private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
    {
        FileFound?.Invoke(this, new FileArgs(e.FullPath, FileOperation.create));

        files = Directory.EnumerateFiles(workingDir).ToList();

        Console.WriteLine($"------------\nПроизошло событие FileSystemWatcher_Created\nВ данный момент самый длинный файл в директории : \n{ProcessFiles()}");
    }

    public void SearchFileInDir()
    {
        foreach (var file in files)
        {
            FileFound?.Invoke(this, new FileArgs(file,FileOperation.none));
        }
    }

    private void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
    {
        FileFound?.Invoke(this,new FileArgs(e.FullPath,FileOperation.update));

        files = Directory.EnumerateFiles(workingDir).ToList();

        Console.WriteLine($"------------\nПроизошло событие FileSystemWatcher_Changed\nВ данный момент самый длинный файл в директории : \n{ProcessFiles()}");


    }

    private void Timer_Elapsed(object? sender, ElapsedEventArgs e)
    {
        timer.Stop();
        Console.WriteLine($"-----------\nПрошло {maxTime}мс , введите 0 для завершения программы или любой другой символ для продолжения");
        if (Console.ReadLine() == "0")
            Stop();




        timer.Start();
    }

    public void Stop()
    {
        isStopProcess = true;
        timer.Stop();
        timer.Elapsed -= Timer_Elapsed;
        fileSystemWatcher.EnableRaisingEvents = false;
        fileSystemWatcher.Changed -= FileSystemWatcher_Changed;
        fileSystemWatcher.Created -= FileSystemWatcher_Changed;
        fileSystemWatcher.Created -= FileSystemWatcher_Changed;
        fileSystemWatcher.Renamed -= FileSystemWatcher_Changed;
        fileSystemWatcher.Dispose();
        timer.Close();


    }


    Func<String, float> f=Extension.ObjectToFloat<String>;

    public string ProcessFiles()
    {
        return files.GetMax<String>(f);
    }

}
