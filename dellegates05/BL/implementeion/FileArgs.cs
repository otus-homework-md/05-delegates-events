﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dellegates05.BL.implementeion
{
    public class FileArgs:EventArgs
    {
        public string? FileName { get; set; }

        public FileOperation WhatHappenWithFile { get; set; }

        public FileArgs(string? fileName, FileOperation whap):base()
        {
            FileName = fileName;
            WhatHappenWithFile = whap;
        }   
    }

    public enum FileOperation
    {
        none,
        create,
        update,
        delete,
        renamed
    }
}
